" non-compatible mode
set nocompatible

" relative line numbers
set number relativenumber

" Leader key
let mapleader=" "

" Key bindings
" Open new tab
map <leader>+ :tabnew<CR>
" Toggle spelling
map <leader>s :set spell!<CR>
" Easy navigate buffers with arrow keys
nmap <leader><Left> :bp<CR>
nmap <leader><Right> :bn<CR>
nmap <leader><Up> :buffers<CR>
nmap <leader><Down> :b
" Map double leader to fuzzy file finder
nmap <leader><leader> :FZF<CR>

" set tabs
set expandtab
set tabstop=4
set shiftwidth=4
set shiftround
set smarttab

" indention
set autoindent
set copyindent

" search
set ignorecase
set smartcase
set hlsearch
set incsearch
" esc to remove highlight after search
" nnoremap <esc> :noh<return><esc>
" use fuzzy finder
set rtp+=/usr/local/opt/fzf

" brackets
set showmatch

" highlighting
syntax on

" spell check
set spell spelllang=en_us

" set character encoding
set encoding=utf-8
set fileencoding=utf-8

" fix double-save problem (see https://superuser.com/questions/1543754/stop-entr-from-running-twice/1569733#1569733)
set backupcopy=yes

" fix ESC key delay
set timeoutlen=1000 ttimeoutlen=0

" colors - color scheme comes with Hyper Terminal
" colo pablo

" word wrap and line breaks - don't break words
set formatoptions=1
set linebreak

" set scrolloff so file over and under cursor is visible for a few lines
set scrolloff=6

" Auto remove trailing white space on save
autocmd BufWritePre * :%s/\s\+$//e

" Enable fzf
set rtp+=/usr/local/opt/fzf

" File type template .html
if has("autocmd")
    augroup templates
        autocmd BufNewFile *.html 0r ~/.vim/templates/skeleton.html
    augroup END
endif

" Auto install plug-ins
if empty(glob('~/.vim/autoload/plug.vim'))
  silent !curl -fLo ~/.vim/autoload/plug.vim --create-dirs
    \ https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
  autocmd VimEnter * PlugInstall --sync | source $MYVIMRC
endif

" Plug-ins
call plug#begin('~/.vim/plugged')

Plug 'itchyny/lightline.vim'
Plug 'itchyny/vim-gitbranch'
Plug 'junegunn/fzf', { 'do': { -> fzf#install() } }
Plug 'ollykel/v-vim'
"Plug 'junegunn/fzf', { 'dir': '~/.fzf', 'do': './install --all' }
Plug 'junegunn/fzf.vim'

call plug#end()

" Activate Light line
set laststatus=2

" Show git info
let g:lightline = {
      \ 'active': {
      \   'left': [ [ 'mode', 'paste' ],
      \             [ 'gitbranch', 'readonly', 'filename', 'modified' ] ]
      \ },
      \ 'component_function': {
      \   'gitbranch': 'gitbranch#name'
      \ },
      \ }
